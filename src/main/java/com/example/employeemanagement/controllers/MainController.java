package com.example.employeemanagement.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")   //So if the homepage or index.html is requested ,  it will come to this controller
public class MainController {

    @RequestMapping(value = "/", method = RequestMethod.GET)   // get method gives you whatever is in the page.
    public String mainPage(){
        return "index";                 //index is the view name
    }
}


